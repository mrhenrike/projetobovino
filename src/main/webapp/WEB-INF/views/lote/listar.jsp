<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Lotes</h1>
<table class="table">
	<tr>
		<th>Fazenda</th>
		<th>�rea</th>
		<th>Data de Inicio</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
	<c:forEach items="${lotes}" var="lote">
		<tr>
			<td>${lote.fazenda.nome}</td>
			<td>${lote.area}</td>
			<td>${lote.inicio}</td>
			<td><a href="/curso-mvc/lote/editar/${lote.id}">Editar</a></td>
			<td><a href="/curso-mvc/lote/apagar/${lote.id}">Apagar</a></td>
		</tr>
	</c:forEach>
</table>

<br />
<a class="btn btn-primary" href="/curso-mvc/lote/cadastrar">Cadastrar
	Lote</a>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>