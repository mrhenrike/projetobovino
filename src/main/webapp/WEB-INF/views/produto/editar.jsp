<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Editar</h1>
<form class="form-horizontal" action="/curso-mvc/produto/atualizar"
	method="post">

	<input type="hidden" name="id" value="${produto.id}">

	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Nome do
			Produto</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="nome"
				value="${produto.nome}">
		</div>
	</div>

	<div class="form-group">
		<label for="quantidade" class="col-sm-2 control-label">Quantidade</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="quantidade"
				value="${produto.quantidade}">
		</div>
	</div>



	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Atualizar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>