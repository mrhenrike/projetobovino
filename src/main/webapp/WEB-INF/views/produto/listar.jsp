<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Produtos</h1>
<table class="table">
	<tr>
		<th>Nome</th>
		<th>Quantidade</th>
		
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
	<c:forEach items="${produtos}" var="produto">
		<tr>
			<td>${produto.nome}</td>
			<td>${produto.quantidade}</td>
			
			<td><a href="/curso-mvc/produto/editar/${produto.id}">Editar</a></td>
			<td><a href="/curso-mvc/produto/apagar/${produto.id}">Apagar</a></td>
		</tr>
	</c:forEach>
</table>

<br />
<a class="btn btn-primary" href="/curso-mvc/produto/cadastrar">Cadastrar
	Produto</a>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>