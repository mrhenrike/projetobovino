<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Funcionarios</h1>
<table class="table">
	<tr>
		<th>Nome</th>
		<th>CPF</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
	<c:forEach items="${funcionarios}" var="funcionario">
		<tr>
			<td>${funcionario.nome}</td>
			<td>${funcionario.cpf}</td>
			<td><a href="/curso-mvc/funcionario/editar/${funcionario.id}">Editar</a></td>
			<td><a href="/curso-mvc/funcionario/apagar/${funcionario.id}">Apagar</a></td>
		</tr>
	</c:forEach>
</table>

<br />
<a class="btn btn-primary" href="/curso-mvc/funcionario/cadastrar">Cadastrar
	Funcionario</a>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>