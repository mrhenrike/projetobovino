<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Editar</h1>
<form class="form-horizontal" action="/curso-mvc/animal/atualizar"
	method="post">

	<input type="hidden" name="id" value="${animal.id}">

	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Nome do animal</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="nome"
				value="${animal.nome}">
		</div>
	</div>

	<div class="form-group">
		<label for="genero" class="col-sm-2 control-label">G�nero do animal</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="genero"
				value="${animal.genero}">
		</div>
	</div>

	<div class="form-group">
		<label for="idade" class="col-sm-2 control-label">Idade do animal</label>
		<div class="col-sm-10">
			<input type="number" class="form-control" name="idade"
				value="${animal.idade}">
		</div>
	</div>
	
	<div class="form-group">
		<label for="peso" class="col-sm-2 control-label">Peso do animal</label>
		<div class="col-sm-10">
			<input type="number" class="form-control" name="peso"
				value="${animal.peso}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Atualizar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>