package br.com.muiraquitec.mvc.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.muiraquitec.mvc.dao.FuncionarioDAO;
import br.com.muiraquitec.mvc.model.Funcionario;


@Repository
public class FuncionarioDAOImpl extends GenericDAOImpl<Long, Funcionario> implements FuncionarioDAO {

	public FuncionarioDAOImpl() {
		super(Funcionario.class);
	}

	@PersistenceContext
	private EntityManager entityManager;

}
