package br.com.muiraquitec.mvc.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.muiraquitec.mvc.dao.FazendaDAO;
import br.com.muiraquitec.mvc.model.Fazenda;

@Repository
public class FazendaDAOImpl extends GenericDAOImpl<Long, Fazenda>implements FazendaDAO {

	public FazendaDAOImpl() {
		super(Fazenda.class);
	}

	public List<Fazenda> buscarProprietario(String nome) {
		Query query = entityManager.createQuery("from Fazenda where proprietario like :filtro");
		query.setParameter("filtro", nome + "%");
		return query.getResultList();
	}

}
