package br.com.muiraquitec.mvc.dao.impl;

import org.springframework.stereotype.Repository;

import br.com.muiraquitec.mvc.dao.LoteDAO;
import br.com.muiraquitec.mvc.model.Lote;

@Repository
public class LoteDAOImpl extends GenericDAOImpl<Long, Lote> implements LoteDAO {

	public LoteDAOImpl() {
		super(Lote.class);
	}
	
}
