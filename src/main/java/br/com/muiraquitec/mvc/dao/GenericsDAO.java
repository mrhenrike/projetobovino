package br.com.muiraquitec.mvc.dao;

import java.util.List;

public interface GenericsDAO<PK, T> {

	public void salvar(T object);
	
	public void atualizar(T object);
	
	public void remover(T object);
	
	public List<T> listar();
	
	public T selecionar(PK pk);
}
