package br.com.muiraquitec.mvc.dao;

import br.com.muiraquitec.mvc.model.Produto;

public interface ProdutoDAO extends GenericsDAO<Long, Produto> {

}
