package br.com.muiraquitec.mvc.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.muiraquitec.mvc.dao.GenericsDAO;

public class GenericsDAOImpl<PK, T> implements GenericsDAO<PK, T>{

	@PersistenceContext
	private EntityManager entityManager;

	private Class<T> clazz;

	public GenericsDAOImpl(Class<T> clazz) {
		this.clazz = clazz;
	}

	public void salvar(T object) {
		entityManager.persist(object);
	}

	public void atualizar(T object) {
		entityManager.merge(object);
	}

	public void remover(T object) {
		entityManager.remove(object);
	}

	public List<T> listar() {
		Query query = entityManager.createQuery("from " + clazz.getName());
		return query.getResultList();
	}

	public T selecionar(PK pk) {
		T object = (T) entityManager.find(clazz, pk);
		return object;
	}

}
