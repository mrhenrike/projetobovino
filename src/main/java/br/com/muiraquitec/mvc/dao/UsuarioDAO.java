package br.com.muiraquitec.mvc.dao;

import br.com.muiraquitec.mvc.model.Usuario;

public interface UsuarioDAO extends GenericsDAO<String, Usuario> {

	Usuario buscarPorEmail(String email);

}
