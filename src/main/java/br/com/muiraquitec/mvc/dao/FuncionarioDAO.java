package br.com.muiraquitec.mvc.dao;

import br.com.muiraquitec.mvc.model.Funcionario;
public interface FuncionarioDAO extends GenericDAO<Long, Funcionario>{

}
