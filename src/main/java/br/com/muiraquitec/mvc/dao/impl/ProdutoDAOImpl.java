package br.com.muiraquitec.mvc.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.muiraquitec.mvc.dao.ProdutoDAO;
import br.com.muiraquitec.mvc.model.Produto;
@Repository
public class ProdutoDAOImpl extends GenericsDAOImpl<Long, Produto> implements ProdutoDAO{

	public ProdutoDAOImpl() {
		super(Produto.class);
		}

	@PersistenceContext
	private EntityManager entityManager;
}
