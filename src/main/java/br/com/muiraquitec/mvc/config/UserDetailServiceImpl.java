package br.com.muiraquitec.mvc.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.muiraquitec.mvc.dao.UsuarioDAO;
import br.com.muiraquitec.mvc.model.Usuario;
@Service
public class UserDetailServiceImpl implements UserDetailsService {
	
	@Autowired
	private UsuarioDAO usuarioDAO;

	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		System.out.println("Pesquisando por email: "+ email);
		Usuario usuario = usuarioDAO.buscarPorEmail(email);		
		if(usuario == null) {
			throw new UsernameNotFoundException("Usu�rio inv�lido"); 
		}		
		System.out.println("Usu�rio encontrado: "+ usuario.getNome());
		return new UserDetailsImpl(usuario);
	}

}
