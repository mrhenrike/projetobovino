package br.com.muiraquitec.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.muiraquitec.mvc.dao.PastoDAO;
import br.com.muiraquitec.mvc.model.Pasto;

@Controller
@Transactional
public class PastoController {

	@Autowired
	private PastoDAO pastoDAO;

	@RequestMapping("/pasto/cadastrar")
	public String cadastrar(Model model) {
		return "/pasto/cadastrar";
	}

	@RequestMapping("/pasto/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		Pasto pasto = pastoDAO.selecionar(id);
		model.addAttribute("pasto", pasto);
		return "/pasto/editar";
	}
	
	@RequestMapping("/pasto/apagar/{id}")
	public String apagar(@PathVariable Long id, Model model) {
		Pasto pasto = pastoDAO.selecionar(id);
		pastoDAO.remover(pasto);
		return "redirect:/pasto/listar";
	}

	@RequestMapping("/pasto/salvar")
	public String salvar(Pasto pasto) {
		pastoDAO.salvar(pasto);
		System.out.println(pasto.getPastagem());
		return "redirect:/pasto/listar";
	}
	
	@RequestMapping("/pasto/atualizar")
	public String atualizar(Pasto pasto) {
		pastoDAO.atualizar(pasto);
		return "redirect:/pasto/listar";
	}

	@RequestMapping("/pasto/listar")
	public String listar(Model model) {
		List<Pasto> pastos = pastoDAO.listar();
		model.addAttribute("pastos", pastos);
		return "/pasto/listar";
	}

	public PastoDAO getPastoDAO() {
		return pastoDAO;
	}

	public void setPastoDAO(PastoDAO pastoDAO) {
		this.pastoDAO = pastoDAO;
	}

}
