package br.com.muiraquitec.mvc.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.*;

/*import br.com.muiraquitec.mvc.model.Pasto;
import javax.persistence.OneToMany;
import java.util.List;*/

@Entity
public class Fazenda {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotBlank
	private String nome;

	@NotBlank
	private String proprietario;

	@DateTimeFormat(pattern="dd/MM/yyyy")
	@NotNull
	@Temporal(TemporalType.DATE)
	private Calendar dataFundacao;

	@OneToMany(mappedBy="fazenda")
	private List<Lote> lotes;
	
	public List<Lote> getLotes() {
		return lotes;
	}

	public void setLotes(List<Lote> lotes) {
		this.lotes = lotes;
	}
	
	 /*@OneToMany(mappedBy = "fazenda")
	 private List<Pasto> pastos;*/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public Calendar getDataFundacao() {
		return dataFundacao;
	}

	public void setDataFundacao(Calendar dataFundacao) {
		this.dataFundacao = dataFundacao;
	}

}
