package br.com.muiraquitec.mvc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/*//Importação Classe Fazenda para Relacionamento
import br.com.muiraquitec.mvc.model.Fazenda;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;*/

@Entity
public class Pasto {

	@Id
	@GeneratedValue
	private Long id;

	private String pastagem;
	
	private String pastagem_tipo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPastagem() {
		return pastagem;
	}

	public void setPastagem(String pastagem) {
		this.pastagem = pastagem;
	}

	public String getPastagem_tipo() {
		return pastagem_tipo;
	}

	public void setPastagem_tipo(String pastagem_tipo) {
		this.pastagem_tipo = pastagem_tipo;
	}
	
	//Declaração do Atributo pra Relacionamento
	/*@ManyToOne
	@JoinColumn(name = "fazenda_id")
	private Fazenda fazenda;*/
	
	
}
